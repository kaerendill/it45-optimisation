/**
 * @file main.c
 * @brief Fonctions de base.
 * @authors Théo Garrido & Enzo Rodrigues
 * @version 1.0
 * @date 22/05/2021
 */
#include "../headers/main.h"
#include "../data/instance-formations.c"

int formationsCopy[NBR_FORMATION][6];
/**
 * Affiche une matrice.
 */
void print_matrix(double d[NBR_NODES][NBR_NODES])
{

  for (int i = 0; i < NBR_NODES; i++)
  {
    printf("%d:", i);
    for (int j = 0; j < NBR_NODES; j++)
    {
      printf("%f ", d[i][j]);
    }
    printf("\n");
  }
}

/**
 * printArray(), Permet d'afficher un tableau en fonction de sa taille.
 */
void printArray(int array[], int size){
  printf("\n[ ");
  for(int i = 0; i<size; i++){
    printf("%d, ", array[i]);
  }
  printf("]\n");
}

/**
* Permet de calculer les distances entres toutes les nodes.
*/
void calc_matrix(double d[NBR_NODES][NBR_NODES], double coord[NBR_NODES][2])
{
  for (int i = 0; i < NBR_NODES; i++)
  {
    for (int j = 0; j < NBR_NODES; j++)
    {
      if (i == j)
      {
        d[i][j] = -1;
      }
      else
      {
        d[i][j] = sqrt(pow((coord[j][0] - coord[i][0]), 2) + pow((coord[j][1] - coord[i][1]), 2));
      }
    }
  }
}

int main(int argc, char *argv[])
{
    int nbIteration;
    if(argc < 2){
        printf("Veuillez entrer le nombre d'itération.\n");
        return 1;
    }else{
        nbIteration = atoi(argv[1]);
    }

  srand(time(NULL)); //Utile pour la génération de nombre aléatoire

  qsort(formations, NBR_FORMATION, sizeof(int)*6, sortIncreasingFormation);

  colony colony = createColony(nbIteration);

  calc_matrix(dist, coord);

  clock_t temps_initial, temps_final; //ms
  double temps_cpu; //s
    
  temps_initial = clock ();

  optimization(&colony);

  temps_final = clock ();
    
  temps_cpu = (double)(temps_final - temps_initial)/CLOCKS_PER_SEC;

  printf("\n");
  if(colony.bestAnt.evaluation != 0){
    int nbInterfaceChomage = printInterfacesWithPath(colony.bestInterfaces);
    printf("BestEval : %f\n", colony.bestAnt.evaluation);
    printf("Moyenne : %f\n", colony.bestAnt.moyenne);
    printf("Ecart Type : %f\n", colony.bestAnt.ecartType);
    printf("Facteur de correlation : %f\n", colony.bestAnt.facCorre);
    printf("nbInterfaceChomage : %d\n", nbInterfaceChomage);
    printf("Nb de spe non satisfaites : %d\n", colony.bestAnt.penaSpe);
    printf("Temps : %lf s", temps_cpu);
    printf("\n");
  }else{
    printf("Aucune solution faisable trouvée\n");
  }
  return 0;
}
