/**
 * @file ant.c
 * @brief Une ant (ou fourimi) représente tous les déplacements faisable par les interfaces. Contient la structure d'une ANT.
 * @authors Théo Garrido & Enzo Rodrigues
 * @version 1.0
 * @date 22/05/2021
 */
#include "../headers/ant.h"
/**
 * createAnt(), Permet d'initialiser une ANT correctement.
 */
void createAnt(ant *self){
    self->path = createListElem(-(NBR_CENTRES_FORMATION+NBR_SESSAD), 0, 0, 0, 0);
    self->startingI = 0;
    self->currentClient = 0;
    self->targetClient = -1;
    self->totalWorkHour = 0;
    self->endFormationHour = 0;
    self->currentDay = 0;
    self->currentSkill = -1;
    self->currentHour = -1;
    self->totalDist = 0;
    self->moyenne = 0;
    self->ecartType = 0;
    self->facCorre = 0;
    self->penaSpe = 0;
    self->evaluation = 0;
}

/**
 * doFirstChoice, Faire le premier choix entre les différents clients grace au tableau cumulatif[].
*/
int doFirstChoice(ant *ant, coupleIndexValue cumulatif[NBR_NODES], int currentDay){

    double randValue = (double) (rand() % 100) / 100;
    for (int j = 0; j < NBR_NODES; j++) {
        if (cumulatif[j].value >= randValue && randValue >= cumulatif[j + 1].value) {
            ant->targetClient = cumulatif[j].index;
            return setHourAndSkillAnt(ant);
        }
    }
    return -1;
}

/**
 * doChoice, Faire un choix de déplacement qui n'est pas le premier, entre les différents clients grace au tableau cumulatif[]. Va modifier le ant.targetClient, ant->totalWorkHour, ant->currentHour, ant->targetClient.
*/
void doChoice(ant *ant, coupleIndexValue cumulatif[NBR_NODES]) {
    double randValue = (double) (rand() % 100) / 100;
    for (int j = 0; j < NBR_NODES; j++) {
        if (cumulatif[j].value > randValue && randValue > cumulatif[j + 1].value) {
            ant->targetClient = cumulatif[j].index;
            int *currentFormation = (int*)malloc(sizeof(int)*6);
            currentFormation = findFormation(ant->startingI, ant->targetClient-(NBR_CENTRES_FORMATION+NBR_SESSAD));
            int duration = currentFormation[5] - currentFormation[4];
            if (ant->totalWorkHour + duration <= 7) {
                ant->totalWorkHour += duration;
                ant->currentHour = currentFormation[5];
            }else{
                ant->targetClient = -1;
            }
            return;
        }
    }
    ant->targetClient = -1;
}

/**
 * moveAnt(), Permet de faire bouger l'ANT, c'est à dire modifier le ant.path qui est le chemins que l'ANT va effectuer.
 */
void moveAnt(ant *ant, double prob[NBR_NODES][NBR_NODES]) {
    int *currentFormation = (int*)malloc(sizeof(int)*6);
    currentFormation = findFormation(ant->startingI, ant->targetClient-(NBR_CENTRES_FORMATION+NBR_SESSAD));
    int indexBestCenter = findNearestCenter(ant->currentClient, currentFormation[1]);
    addTail(ant->path, createListElem(ant->targetClient-(NBR_CENTRES_FORMATION+NBR_SESSAD), ant->targetClient, currentFormation[3], currentFormation[4], currentFormation[5]));
    addTail(ant->path, createListElem(indexBestCenter-(NBR_SESSAD + NBR_CENTRES_FORMATION), indexBestCenter, currentFormation[3], currentFormation[4], currentFormation[5]));
    addTail(ant->path, createListElem(ant->targetClient-(NBR_CENTRES_FORMATION+NBR_SESSAD), ant->targetClient, currentFormation[3], currentFormation[4], currentFormation[5]));
    ant->currentClient = ant->targetClient;
    ant->totalDist += dist[ant->currentClient][ant->targetClient] + dist[ant->targetClient][indexBestCenter]*2;
    for (int i = 0; i < NBR_NODES; i++) {
        prob[i][ant->targetClient] = -1;
    }
}

/**
 *  createWay(), Créer un chemins pour une ANT en utilisant **doChoice()** et **moveAnt()**.
 */
ant createWay(ant *ant, double prob[NBR_NODES][NBR_NODES], double pheromone[NBR_NODES][NBR_NODES], coupleIndexValue cumulatif[NBR_NODES]) {
    if (ant->currentHour == 12) {
        ant->currentHour++;
    } else {
        cumulativProba(prob, cumulatif, ant->currentClient);

        int nextChoice = 0;

        doChoice(ant, cumulatif);

        if (ant->targetClient == -1) {
            ant->currentHour++;
        } else {
            moveAnt(ant, prob);
        }
    }
    updateProbaMatrix(ant->currentClient, ant->currentDay, ant->currentHour, ant->currentSkill, prob, pheromone);
    return *ant;
}


/**
 * buildSolution(),Le path final d'une ANT va être divisé et redistribué aux interfaces en prenant en compte leurs compétences, spécialités, temps de travail global et aussi en s'assurant que l'interface ne soit pas déjà avec un client.
 */
int buildSolution(ant *ant, interface interfaces[NBR_INTERFACES]) {

    list tmp = ant->path;

    int nbSessad = 0;
    int distanceSubPath = 0;
    int currentSkill;
    int currentWorkHour = 0;

    int *currentFormation = (int*)malloc(sizeof(int)*6);

    int eachSpe[NBR_SPECIALITES];

    int penaSpe = 0;

    for (int i = 0; i < NBR_SPECIALITES; ++i) {
        eachSpe[i] = 0;
    }


    list subPath = NULL;
    int day = 0;

    while(tmp != NULL){
        subPath = addTail(subPath, createListElem(tmp->idClient, tmp->indexClient, tmp->jourClient, tmp->startingHour, tmp->endingHour));

        if(tmp->idClient > -(NBR_SESSAD+NBR_CENTRES_FORMATION) && tmp->idClient < 0){
            subPath = addTail(subPath, createListElem(tmp->next->idClient, tmp->next->indexClient, tmp->next->jourClient, tmp->next->startingHour, tmp->next->endingHour));
            subPath = addTail(subPath, createListElem(tmp->next->next->idClient, tmp->next->next->indexClient, tmp->next->next->jourClient, tmp->next->next->startingHour, tmp->next->next->endingHour));
            tmp=tmp->next->next;
        }

        if(tmp->idClient == -(NBR_CENTRES_FORMATION + NBR_SESSAD)){


            nbSessad++;
            if(nbSessad == 2){
                if(!affectSubPathWithInterface(interfaces, distanceSubPath, currentSkill, currentWorkHour, eachSpe, subPath, day, &penaSpe)){
                    return 0;
                }
                subPath = NULL;
                subPath = addTail(subPath, createListElem(tmp->idClient, tmp->indexClient, tmp->jourClient, tmp->startingHour, tmp->endingHour));
                distanceSubPath = 0;
                currentSkill = -1;
                currentWorkHour = 0;
                for (int i = 0; i < NBR_SPECIALITES; ++i) {
                    eachSpe[i] = 0;
                }
                nbSessad = 1;
            }

            if(tmp->next->idClient == -(NBR_CENTRES_FORMATION + NBR_SESSAD)){
                tmp = tmp->next;
                nbSessad = 1;
                day++;
            }

            if(tmp->next != NULL){
                distanceSubPath += dist[0][tmp->next->indexClient];
            }


        }else{
            currentFormation = findFormation(0,tmp->idClient);
            currentSkill = currentFormation[2];

            eachSpe[currentFormation[1]]++; //ajoute +1 à la spe associe
            currentWorkHour += currentFormation[5] - currentFormation[4];
            distanceSubPath += dist[tmp->indexClient][findNearestCenter(tmp->indexClient, currentFormation[1])]*2; //*2 aller et retour

            if(tmp->next->next->next == NULL){
                distanceSubPath += dist[tmp->indexClient][tmp->next->next->indexClient];
            }else{
                distanceSubPath += dist[tmp->indexClient][tmp->next->next->next->indexClient];
            }

        }
        tmp = tmp->next;
    }

    double moy = calcMoy(ant->totalDist);
    double ecartType = calcEcartType(moy, interfaces);
    double facCorre = calcFacCorre(ant->totalDist);

    ant->evaluation = calculEvaluation(moy, ecartType, facCorre, penaSpe);
    ant->moyenne = moy;
    ant->ecartType = ecartType;
    ant->facCorre = facCorre;
    ant->penaSpe = penaSpe;

    return 1;
}

/**
 *  setHourAndSkillAnt(), Met à jour les infos de la ANT sur ses compétences, son heure actuel et son nombre d'heure de travail cumulé.
 */
int setHourAndSkillAnt(ant *ant){
    for(int i = 0; i<NBR_FORMATION; i++){
        if(ant->currentDay < formationsCopy[i][3]){
            return -1;
        }else if(ant->currentDay == formationsCopy[i][3] && ant->targetClient-(NBR_CENTRES_FORMATION + NBR_SESSAD) == formationsCopy[i][0]){
            ant->currentHour = formationsCopy[i][5];
            ant->totalWorkHour = formationsCopy[i][5] - formationsCopy[i][4];
            ant->currentSkill = formationsCopy[i][2];
            return 1;
        }
    }
    return -1;
}

/**
 * affectSubPathWithInterface(), Permet d'affecter un sous chemin du path final de l'ANT à une interface à l'aide de **calculScore()**.
 * 
 */
int affectSubPathWithInterface(interface interfaces[NBR_INTERFACES], int distanceSubPath, int currentSkill, int totalWorkHour, int eachSpe[NBR_SPECIALITES], list subPath, int day, int *penaCount){
    int iBestInterface = -1;
    interface interface;
    double bestScore = 0;
    double score = 0;
    for(int i =0; i<NBR_INTERFACES; i++){
        interface = interfaces[i];
        score = calculScore(interface, currentSkill, eachSpe, day);
        if(score > bestScore){
            bestScore = score;
            iBestInterface = i;
        }
    }
    if(iBestInterface == -1){
        return 0;
    }
    interfaces[iBestInterface].totalDistance += distanceSubPath;
    interfaces[iBestInterface].totalWorkHour += totalWorkHour;
    interfaces[iBestInterface].path = addTail(interfaces[iBestInterface].path, subPath);
    interfaces[iBestInterface].workingDays[day]++;

    for (int i = 0; i < NBR_SPECIALITES; ++i) {
        if(eachSpe[i] != 0 && interfaces[iBestInterface].specialities[i] == 0){
            *penaCount += eachSpe[i];
        }
    }

    return 1;
}

/**
 * findNearestCenter(), Permet de trouver le centre avec la bonne compétence le plus proche du client.
 */
 int findNearestCenter(int currentClientIndex, int currentSpe){
     double bestDist = INFINITY;
     int bestCenterIndex = -1;
    for(int i = 0; i < NBR_CENTRES_FORMATION; i++){
        if(list_center[i] == currentSpe && dist[currentClientIndex][i + NBR_SESSAD] < bestDist){
            bestDist = dist[currentClientIndex][i + NBR_SESSAD];
            bestCenterIndex = i + NBR_SESSAD;
        }
    }
    return bestCenterIndex;
 }

/**
 * calculScore(), Permet de calculer le score d'une interface c'est à dire, à quel point une interface est bonne pour un sous chemins.
 * 
 */
int calculScore(interface interface, int currentSkill, int eachSpe[NBR_SPECIALITES], int day){
    int score = 0;
    int countSpe = 0;
    for(int i = 0; i<NBR_SPECIALITES; i++){
        countSpe += eachSpe[i];
    }
    int valueSpe = (int)50/countSpe;
    if(interface.skills[currentSkill] && interface.workingDays[day] == 0 && interface.totalWorkHour < 35){
        score+=50;
    }else{
        return -100;
    }
    for(int i = 0; i<NBR_SPECIALITES; i++){
        if(interface.specialities[i] == 1 && eachSpe[i] == 0){
            score-=(int)valueSpe/2;
        }
        score += interface.specialities[i] * eachSpe[i] * valueSpe;
    }
    return score;
}

/**
 * calcMoy(), Renvois la distance moyenne parcourue par toutes les interfaces de la solution.
 *
 */
double calcMoy(double distanceTotal){
    return distanceTotal/NBR_INTERFACES;
}

/**
 * calcEcartType(), Renvois l'ecart type des distances de la solution.
 *
 */
double calcEcartType(double moyenneDistance,interface interfaces[NBR_INTERFACES]){

    double ecartType = 0;
    double somme = 0;

    for (int i = 0; i < NBR_INTERFACES; ++i) {
        somme += pow(interfaces[i].totalDistance - moyenneDistance, 2);
    }

    ecartType = sqrt(somme/NBR_INTERFACES);

    return ecartType;

}

/**
 * calcFacCorre(), Renvois le facteur de corrélation de la solution.
 *
 */
double calcFacCorre(double distanceTotal){
    return distanceTotal/NBR_FORMATION;
}

/**
* calculEvaluation(), Calcule de l'evaluation (minZ) d'une ANT.
*/
double calculEvaluation(double moyenne, double ecartType, double facCorre, int penalite){
    return 0.5*(moyenne+ecartType) + 0.5*facCorre*penalite;
}