/**
 * @file list.c
 * @brief Liste pointe vers le premier **listElem**.
 * @authors Théo Garrido & Enzo Rodrigues
 * @version 1.0
 * @date 22/05/2021
 */
#include "../headers/list.h"

/**
 * addTail(), Permet d'ajouter un élément en queue.
 */
list addTail(list self, listElem *elem){
    list tmp = self;
    if(self == NULL){
        self = elem;
    }else{
        while(tmp->next != NULL){
            tmp = tmp->next;
        }
        tmp->next = elem;
    }

    return self;
}

/**
 * printList(), Permet d'afficher une liste.
 */
void printList(list self){
    list tmp = self;
    int i = 0;
    while(tmp != NULL){
        printf("%d : ", i);
        i++;
        printElem(tmp);
        tmp = tmp->next;
    }
}

/**
 * findClientByIndex(), Permet de récupérer un client par son index.
 */

int findClientByIndex(list self, int index){
    list tmp = self;
    int i = 0;
    while(i < index && tmp != NULL){
        i++;
        tmp = tmp->next;
    }

    return i;
}