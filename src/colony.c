/**
 * @file colony.c
 * @brief Permets de gérer toutes les ANT Générés et donc de récupérer la meilleure des ANT qui a la meilleure solution minZ.
 * @authors Théo Garrido & Enzo Rodrigues
 * @version 1.0
 * @date 22/05/2021
 */
#include "../headers/colony.h"

/**
 * createColony(), Permet de créer une colonie.
 */

colony createColony(int nbAnt) {
    colony colony;
    ant bestAnt;
    bestAnt.evaluation = INFINITY;
    colony.nbAnt = nbAnt;
    colony.bestAnt = bestAnt;
    for (int i = 0; i < NBR_NODES; i++) {
        for (int j = 0; j < NBR_NODES; j++) {
            if (i == j || j < NBR_CENTRES_FORMATION + 1) {
                colony.pheromone[i][j] = -1.0;
            } else {
                colony.pheromone[i][j] = 1.0;
            }
        }
    }
    return colony;
}

/**
 * evaporatePheromone(), Évapore les phéromones.
 **/

void evaporatePheromone(double pheromone[NBR_NODES][NBR_NODES]) {
    for (int i = 0; i < NBR_NODES; i++) {
        for (int j = 0; j < NBR_NODES; j++) {
            if (pheromone[i][j] != -1) {
                if(pheromone[i][j] * (1 - RHO) > MINPHE){
                    pheromone[i][j] *= (1 - RHO);
                }else{
                    pheromone[i][j] = MINPHE;
                }
            }
        }
    }
}

/**
 * updatePheromone(), Mets à jour la matrice des phéromones.
 **/
void updatePheromone(ant ant, double pheromone[NBR_NODES][NBR_NODES]) {
    list tmp = ant.path;
    int lasClient = -1;
    while (tmp->next->next->next != NULL) {

        if (tmp->idClient != lasClient && tmp->idClient >= 0 && pheromone[tmp->indexClient][tmp->next->next->next->indexClient] != -1) {

            if(pheromone[tmp->indexClient][tmp->next->next->next->indexClient] + 1 / ant.evaluation < MINPHE){

                pheromone[tmp->indexClient][tmp->next->next->next->indexClient] = MINPHE;

            }else{

                pheromone[tmp->indexClient][tmp->next->next->next->indexClient] += 1 / ant.evaluation;

            }

            lasClient = tmp->idClient;
        }

        tmp = tmp->next;
    }
}

/**
 * optimization(), Lance l'optimisation.
 */
void optimization(colony *colony) {
    coupleIndexValue cumulatif[NBR_NODES];
    double prob[NBR_NODES][NBR_NODES];

    for (int x = 0; x < colony->nbAnt; x++) {
        interface interfaces[NBR_INTERFACES];
        initInterfaces(interfaces);
        ant ant;
        createAnt(&ant);
        initProbaMatrix(prob);
        int startingI = 0;
        memcpy(formationsCopy, formations, NBR_FORMATION * 6 * sizeof(int));


        for (int currentDay = 1; currentDay < 7; currentDay++) {
            startingI = findIndexFirstFormationDay(startingI, currentDay);
            ant.currentClient = 0;
            ant.currentDay = currentDay;
            ant.startingI = startingI;
            updateProbaMatrix(ant.currentClient, ant.currentDay, ant.currentHour, ant.currentSkill, prob,
                              colony->pheromone);
            cumulativProba(prob, cumulatif, ant.currentClient);
            int heureDebutDay = findFirstHourOfDay(startingI, currentDay);
            if (doFirstChoice(&ant, cumulatif, currentDay) == -1) {
                continue;
            } else {
                moveAnt(&ant, prob);
            }

            int isRemainingFormation = 1;
            while (ant.currentHour < heureDebutDay + 12 && isRemainingFormation) {

                ant = createWay(&ant, prob, colony->pheromone, cumulatif);

                if (heureDebutDay + 12 == ant.currentHour) {
                    isRemainingFormation = isRemainingFormationByDay(startingI, currentDay, prob[0]);
                    if (isRemainingFormation) {
                        ant.currentClient = 0;
                        addTail(ant.path,
                                createListElem(-(NBR_CENTRES_FORMATION + NBR_SESSAD), 0, ant.currentDay, 0, 0));
                        updateProbaMatrix(ant.currentClient, ant.currentDay, ant.currentHour, ant.currentSkill, prob,
                                          colony->pheromone);
                        cumulativProba(prob, cumulatif, ant.currentClient);
                        if (doFirstChoice(&ant, cumulatif, currentDay) == -1) {
                            continue;
                        } else {
                            moveAnt(&ant, prob);
                        }
                    }
                }
            }
            addTail(ant.path, createListElem(-(NBR_CENTRES_FORMATION + NBR_SESSAD), 0, ant.currentDay, 0, 0));
        }

        if (!buildSolution(&ant, interfaces)) {
            //printf("Solution infaisable\n");
            ant.evaluation = -ant.totalDist / 2;

        } else {
            if (ant.evaluation < colony->bestAnt.evaluation) {
                colony->bestAnt = ant;
                for (int i = 0; i < NBR_INTERFACES; ++i) {
                    colony->bestInterfaces[i] = interfaces[i];
                }
                printf("\nNouvelle solution = %f", colony->bestAnt.evaluation);
            }
        }


        evaporatePheromone(colony->pheromone);
        updatePheromone(ant, colony->pheromone);
    }
}