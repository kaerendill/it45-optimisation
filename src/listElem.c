/**
 * @file listElem.c
 * @brief Element d'une liste.
 * @authors Théo Garrido & Enzo Rodrigues
 * @version 1.0
 * @date 22/05/2021
 */
#include "../headers/listElem.h"

/**
 * createListElem(), Permet de créer un élément de liste.
 */
listElem* createListElem(int idClient, int indexClient, int jourClient, int startingHour, int endingHour){
    listElem *self = (listElem*)malloc(sizeof(listElem));
    self->idClient = idClient;
    self->indexClient = indexClient;
    self->jourClient = jourClient;
    self->startingHour = startingHour;
    self->endingHour = endingHour;
    self->next = NULL;
    return self;
}

/**
 * printElem(), Permet de créer un élément de liste.
 */
void printElem(listElem* self){
    printf("Id du client = %d,  Jour : %d, heureDebut : %d, heureFin : %d, next = %p\n", self->idClient, self->jourClient, self->startingHour, self->endingHour, self->next);
}