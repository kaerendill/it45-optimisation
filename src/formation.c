/**
 * @file formation.c
 * @brief Gère tout ce qui est en rapport avec les formations.
 * @authors Théo Garrido & Enzo Rodrigues
 * @version 1.0
 * @date 22/05/2021
 */
#include "../headers/formation.h"
#include <stdlib.h>
#include <stdio.h>

/**
 * sortIncreasingFormation(), Trie les formations par heure et par jour par ordre croissant.
*/
int sortIncreasingFormation(const void *a, const void *b) {
    int *casta = (int *) malloc(6 * sizeof(int));
    casta = (int *) a;

    int *castb = (int *) malloc(6 * sizeof(int));
    castb = (int *) b;

    int jour_a = casta[3];
    int heureDepart_a = casta[4];
    int jour_b = castb[3];
    int heureDepart_b = castb[4];

    if (jour_a == jour_b && heureDepart_a == heureDepart_b) {
        return 0;
    } else if (jour_a == jour_b && heureDepart_a > heureDepart_b || jour_a > jour_b) {
        return 1;
    } else if (jour_a == jour_b && heureDepart_a < heureDepart_b || jour_a < jour_b) {
        return -1;
    }
}

/**
 * findFirstoHourOfDay(), permet de retourner la première formation du jour
 */

int findFirstHourOfDay(int startingI, int currentDay){
    for(int i=startingI; i<NBR_FORMATION; i++){
        if(currentDay < formationsCopy[i][3]){
            return -1;
        }else if(currentDay == formationsCopy[i][3]){
            return formationsCopy[i][4];
        }
    }
}

/**
 * durationFormation(), Permet d'obtenir la durée d'une formation.
 */

int durationFormation(int startingI, int targetClient, int currentDay, int currentHour) {
    for (int i = startingI; i < NBR_FORMATION; i++) {
        if(currentDay < formationsCopy[i][3]){
            return -1;
        }else if (formationsCopy[i][0] == targetClient - (NBR_CENTRES_FORMATION + NBR_SESSAD)) {
            return formationsCopy[i][5] - formationsCopy[i][4];
        }
    }
    return -1;
}

/**
 * findIndexFirstFormationDay(), permet de retourner l'index où commencent les formations d'un jour donné
 * 
 */

int findIndexFirstFormationDay(int startingI, int currentDay){
    for(int i=startingI; i<NBR_FORMATION; i++){
        if(currentDay == formationsCopy[i][3]){
            return i;
        }
    }
}

/**
* findFormation(), return une formation
*/
int *findFormation(int startingI, int idClient){
    for (int i = startingI; i < NBR_FORMATION; ++i) {
        if(formations[i][0] == idClient){
            return formations[i];
        }
    }
}