/**
 * @file proba.c
 * @brief Toutes les fonctions en rapport avec les probabilités de choix de chemins.
 * @authors Théo Garrido & Enzo Rodrigues
 * @version 1.0
 * @date 22/05/2021
 */
#include "../headers/proba.h"

/**
* Calcul du denominateur pour la proba.
*/
double denoCalc(int currentClient, int currentDay, int currentHour, int currentSkill, double pheromone[NBR_NODES][NBR_NODES], double prob[NBR_NODES][NBR_NODES])
{
  double deno = 0;
  for (int j = 0; j < NBR_NODES; j++)
  {
    if (dist[currentClient][j] != -1 && prob[currentClient][j] != -1 && possibleWayAnt(j, currentClient, currentDay, currentHour, currentSkill))
    {
      deno += pow(pheromone[currentClient][j], A) * pow(1 / dist[currentClient][j], B);
    }
  }
    return deno;
}

/**
* initProbaMatrix, Initialise la matrice de proba.
*/
void initProbaMatrix(double prob[NBR_NODES][NBR_NODES])
{

    for (int i = 0; i < NBR_NODES; i++)
    {
        for (int j = 0; j < NBR_NODES; j++)
        {
            if (dist[i][j] == -1 || j < NBR_CENTRES_FORMATION+NBR_SESSAD)
            {
                prob[i][j] = -1;
            }
            else
            {
                prob[i][j] = 0;
            }
        }
    }
}

/**
* updateProbaMatrix Update la matrice de proba.
*/
void updateProbaMatrix(int currentClient, int currentDay, int currentHour, int currentSkill, double prob[NBR_NODES][NBR_NODES], double pheromone[NBR_NODES][NBR_NODES]){
  for(int j = 0; j < NBR_NODES; j++)
  {
    if(prob[currentClient][j] != -1 && possibleWayAnt(j, currentClient, currentDay, currentHour, currentSkill)){
      double deno = denoCalc(currentClient, currentDay, currentHour, currentSkill, pheromone, prob);
      prob[currentClient][j] = (pow(pheromone[currentClient][j], A) * (pow(1 / dist[currentClient][j], B))) / deno;
    }
  }
}

/**
 * sortDecreasing, Permet de trier **b** toujours supérieur a **a**.
*/
int sortDecreasing(const void *a, const void *b)
{

  coupleIndexValue *coupleIndexValue_a = ((coupleIndexValue *)a);
  coupleIndexValue *coupleIndexValue_b = ((coupleIndexValue *)b);

  if (coupleIndexValue_a->value == coupleIndexValue_b->value)
  {
    return 0;
  }
  else if (coupleIndexValue_a->value > coupleIndexValue_b->value)
  {
    return -1;
  }
  else
  {
    return 1;
  }
}

/**
 * cumulativProba matrice qui va permettre de choisir un chemin en fonction d'un nombre compris entre {0,1}.
*/
void cumulativProba(double prob[NBR_NODES][NBR_NODES], coupleIndexValue cumulatif[NBR_NODES], int currentClient)
{

  coupleIndexValue tempArray[NBR_NODES];

  for (int j = 0; j < NBR_NODES; j++)
  {
    coupleIndexValue strucTemp = {j, prob[currentClient][j]};
    tempArray[j] = strucTemp;
  }

  qsort(tempArray, NBR_NODES, sizeof(coupleIndexValue), sortDecreasing);


  for (int k = 0; k < NBR_NODES; k++)
  {
    cumulatif[k].index = tempArray[k].index;
    cumulatif[k].value = 0;
    for (int j = k; j < NBR_NODES; j++)
    {
      if (tempArray[j].value != -1)
      {
        cumulatif[k].value += tempArray[j].value;
      }
    }
  }
}

/**
 * isRemainingFormationByDay(), Permet de savoir s'il reste des formations un jour donné.
 */
int isRemainingFormationByDay(int startingI, int currentDay, double firstLineProb[NBR_NODES]){
  for(int i=startingI; i < NBR_NODES; i++){
    if(currentDay < formationsCopy[i][3]){
      return 0;
    }else if(currentDay == formationsCopy[i][3] && formationsCopy[i][0] != -1){
      return 1;
    }
  }
  return 0;
}

/**
 * possibleWayAnt(), permet de vérifier pour une ville i si elle est synchroniser avec une fourmi k pour un jour j
 *
 */

int possibleWayAnt(int indexClient, int currentClient, int currentDay, int currentHour, int currentSkill) {
    int i = 0;
    for (i; i < NBR_FORMATION; i++) {
        if (formationsCopy[i][0] == indexClient - (NBR_CENTRES_FORMATION + NBR_SESSAD)) {
            if (formationsCopy[i][3] > currentDay) {
                return 0;
            } else if (formationsCopy[i][3] == currentDay && formationsCopy[i][4] > currentHour && formationsCopy[i][2] == currentSkill || currentClient == 0) {
                return 1;
            }
        }
    }
    return 0;
}