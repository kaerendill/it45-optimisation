/**
 * @file interface.c
 * @brief Gestion des interfaces en lisant les fichiers générés **instance-formation**.
 * @authors Théo Garrido & Enzo Rodrigues
 * @version 1.0
 * @date 22/05/2021
 */


#include "../headers/interface.h"

/**
* createInterface(), Permet de cree une interface depuis le fichier **instance-formation**.
*/
interface createInterface(int id, int skills[NBR_SKILLS], int spes[NBR_SPECIALITES]){
    interface self;
    self.id = id;
    memcpy(self.skills, skills, sizeof(int) * NBR_SKILLS);
    memcpy(self.specialities, spes, sizeof(int) * NBR_SPECIALITES);
    self.totalWorkHour = 0;
    self.totalDistance = 0;
    self.path = NULL;
    for(int i=0; i<6; i++){
        self.workingDays[i] = 0;
    }
    return self;
}

/**
 * printInterfaces(), Affiche les interfaces.
 */
void printInterfaces(interface interfaces[NBR_INTERFACES]){
    for(int i = 0; i < NBR_INTERFACES; i++){
        printf("Interface Numero %d\n", interfaces[i].id);
        printf("skill[ ");
        for (int j = 0; j < NBR_SKILLS; ++j) {
            printf("%d, ", interfaces[i].skills[j]);
        }
        printf("]\n");
        printf("specialities[ ");
        for (int j = 0; j < NBR_SPECIALITES; ++j) {
            printf("%d, ", interfaces[i].specialities[j]);
        }
        printf("]\n");
        printf("totalWorkHour = %d\n", interfaces[i].totalWorkHour);
        printf("totalDistance = %f\n", interfaces[i].totalDistance);
        printList(interfaces[i].path);
        printf("\n");
    }
}

/**
 * printInterfacesWithPath(), Affiche les interfaces eyant un chemins.
 */
int printInterfacesWithPath(interface interfaces[NBR_INTERFACES]){
    int nbInterfaceChomage = 0;
    for(int i = 0; i < NBR_INTERFACES; i++){
        if(interfaces[i].totalDistance != 0){
            printf("Interface Numero %d\n", interfaces[i].id);
            printf("skill[ ");
            for (int j = 0; j < NBR_SKILLS; ++j) {
                printf("%d, ", interfaces[i].skills[j]);
            }
            printf("]\n");
            printf("specialities[ ");
            for (int j = 0; j < NBR_SPECIALITES; ++j) {
                printf("%d, ", interfaces[i].specialities[j]);
            }
            printf("]\n");
            printf("totalWorkHour = %d\n", interfaces[i].totalWorkHour);
            printf("totalDistance = %f\n", interfaces[i].totalDistance);
            printList(interfaces[i].path);
            printf("\n");
        }else{
            nbInterfaceChomage++;
        }
    }

    return nbInterfaceChomage;

}

/**
 * initInterfaces(), Ajoute chaque interface dans le tableau interfaces.
 */
void initInterfaces(interface interfaces[NBR_INTERFACES]){
    for (int i = 0; i < NBR_INTERFACES; ++i) {
        interfaces[i] = createInterface(i, competences_interfaces[i], specialite_interfaces[i]);
    }
}
