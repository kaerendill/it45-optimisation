# Les librairies que l'on va utiliser
LIBS= -lm

# Les endroits oe on peut trouver des librairies
LIBS_PATH= \
	-L/lib \
	-L/usr/lib \
	-L/usr/local/lib

# Le compilateur que nous utiliserons #g++
COMPILO=gcc

# Les options de compilation des sources
#    -c    : Ne fait pas un executable, mais seulement un .o
#    -g    : Fait un objet debuggable
FLAGS= -c -g

# Liste des objets que l'on va obtenir, et qu'il faudra linker.
OBJ= \
	obj/listElem.o \
	obj/list.o \
	obj/formation.o \
	obj/interface.o \
	obj/proba.o \
	obj/ant.o \
	obj/colony.o \
	obj/main.o 

######################
#                    #
# Cibles Principales #
#                    #
######################

all: clean bin/main

# Pour compiler coup_de_vent, il faut verifier que tous les .o
# sont e jour (les recompiler dans le cas contraire) puis lancer
# la commande de linkage.
bin/main : $(OBJ) 
	$(COMPILO) -o $@ $^ $(LIBS)

# Taper 'make clean' provoque l'effacement de tous les .o, et des *~ laisses 
# par emacs. Il n'y a pas de dependence pour cette cible.
clean : 
	rm -rf obj/*.o

# Taper 'make clear' fait un 'make clean' puis efface en plus l'executable.
clear : 
	make clean;
	rm -f main


###########################
#                         #
# Compilation des sources #
#                         #
###########################

# Source bidon, bien utile pour faire des copier-coller et obtenir les suivantes
# par en remplacement de xx.
# Le sens est que xx.o s'obtent e partir de xx.cc (la seule dependance ici) quand
# le fichier xx.cc vient d'etre modifie
obj/%.o : src/%.c
	$(COMPILO) $(FLAGS) -o $@ -c $<  

#xx.o : xx.cc
#	$(COMPILO) $(FLAGS) $(INCLUDE_PATH) -o xx.o xx.cc

