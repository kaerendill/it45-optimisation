/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package instancegenerator;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Arrays;

/**
 *
 * @author Olivier Grunder
 */
public class InstanceGenerator {

//    public static int NBR_APPRENANTS = 20;
//    public static int NBR_FORMATIONS_PAR_SEMAINE = 4; // max 10 demi-journées par apprenant

    public static int NBR_APPRENANTS = 80;

    public static int NBR_FORMATIONS_PAR_SEMAINE = 1;

    public static int DIMENSION_ZONE_GEOGRAPHIQUE = 200;

    public static int NBR_INTERFACES;
    public static int NBR_FORMATIONS;

    public static String HEADERFILENAME = "./data/instance-formations.h";
    public static String SOURCEFILENAME = "./data/instance-formations.c";

    public static int NBR_COMPETENCES = 2;
    public static String NOMS_COMPETENCES[] = {
        "COMPETENCE_SIGNES",
        "COMPETENCE_CODAGE"};

    public static int NBR_CENTRES_FORMATION = 3; 
    public static int NBR_SPECIALITES = 3;
    public static String NOMS_SPECIALITES[] = {
        "SPECIALITE_MENUISERIE",
        "SPECIALITE_ELECTRICITE",
        "SPECIALITE_MECANIQUE",
        "SPECIALITE_INFORMATIQUE",
        "SPECIALITE_CUISINE",
        "SPECIALITE_COMMERCIAL",
        "SPECIALITE_OENOLOGIE",
        "SPECIALITE_PLOMBERIE",
        "SPECIALITE_RESEAUX",
        "SPECIALITE_HOTELLERIE"
    };

    public static String JOURS_SEMAINE[] = {
        "LUNDI",
        "MARDI",
        "MERCREDI",
        "JEUDI",
        "VENDREDI",
        "SAMEDI"};

    protected BufferedWriter headerFileOutput;
    protected BufferedWriter sourceFileOutput;

    Random rand;

    public InstanceGenerator(int nbr_appre, int dim_zone_geo, int nbr_spe, int nbr_centres) {
        rand = new Random();
        try {
            NBR_APPRENANTS = nbr_appre;
            DIMENSION_ZONE_GEOGRAPHIQUE = dim_zone_geo;
            NBR_SPECIALITES = nbr_spe;
            NBR_CENTRES_FORMATION = nbr_centres;
            NBR_FORMATIONS = NBR_APPRENANTS * NBR_FORMATIONS_PAR_SEMAINE;
            NBR_INTERFACES = (int) (NBR_APPRENANTS/4 * 1.2);

            headerFileOutput = new BufferedWriter(new FileWriter(HEADERFILENAME));
            sourceFileOutput = new BufferedWriter(new FileWriter(SOURCEFILENAME));

            writeHeader();
            writeCompetencesInterfaces();
            writeSpecialiteInterfaces();
            writeCoord();
            writeFormation();
            writeGlobal();

            headerFileOutput.flush();
            sourceFileOutput.flush();
            sourceFileOutput.close();
            headerFileOutput.close();
        } catch (IOException ex) {
            Logger.getLogger(InstanceGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //#include <stdio.h>
    //
    //#define NBR_INTERFACES        10 
    //#define NBR_APPRENANTS	       8 
    //#define NBR_CENTRES_FORMATION  3 
    //#define NBR_SPECIALITES        3 
    //#define NBR_NODES 	       NBR_CENTRES_FORMATION+NBR_INTERFACES+NBR_APPRENANTS
    //
    ///* code des compétence en langage des signes et en codage LPC */
    //#define COMPETENCE_SIGNES      0 
    //#define COMPETENCE_CODAGE      1 
    private void writeHeader() {
        try {
            headerFileOutput.write("#include <stdio.h>\n");
            headerFileOutput.write("                  \n");
            headerFileOutput.write("#define NBR_INTERFACES        " + NBR_INTERFACES + "\n");
            headerFileOutput.write("#define NBR_APPRENANTS        " + NBR_APPRENANTS + "\n");
            headerFileOutput.write("#define NBR_FORMATIONS        " + NBR_FORMATIONS + "\n");
            headerFileOutput.write("#define NBR_CENTRES_FORMATION " + NBR_CENTRES_FORMATION + "\n");
            headerFileOutput.write("#define NBR_SPECIALITES       " + NBR_SPECIALITES + "\n");
            headerFileOutput.write("#define NBR_NODES 	      NBR_CENTRES_FORMATION+NBR_INTERFACES+NBR_APPRENANTS\n");
            headerFileOutput.write("                  \n");
            headerFileOutput.write("/* code des compétence en langage des signes et en codage LPC */\n");
            headerFileOutput.write("#define COMPETENCE_SIGNES     0\n");
            headerFileOutput.write("#define COMPETENCE_CODAGE     1\n");
            headerFileOutput.write("                  \n");
            sourceFileOutput.write("#include \"./instance-formations.h\"\n");
        } catch (IOException ex) {
            Logger.getLogger(InstanceGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    ///* competences des interfaces en SIGNES et CODAGE*/
    //int competences_interfaces[NBR_INTERFACES][2]={
    //    {1,0}, /* compétence en langages des SIGNES mais pas en CODAGE LPC */
    //    {0,1}, /* pas de compétence en langages des SIGNES mais compétence en CODAGE LPC */
    //    {0,1},
    //    {1,0},
    //    {0,1},
    //    {1,0},
    //    {1,0},
    //    {1,0},
    //    {0,1},
    //    {1,0}
    //};
    private void writeCompetencesInterfaces() {
        try {
            sourceFileOutput.write("/* competences des interfaces en SIGNES et CODAGE*/\n");
            sourceFileOutput.write("int competences_interfaces[NBR_INTERFACES][2]={\n");
            sourceFileOutput.write("    {1,0}, /* compétence en langages des SIGNES mais pas en CODAGE LPC */\n");
            sourceFileOutput.write("    {0,1}, /* pas de compétence en langages des SIGNES mais compétence en CODAGE LPC */\n");
            int maxi = NBR_INTERFACES - 2;
            for (int i = 0; i < maxi; i++) {
		double f = rand.nextDouble() ;
                if (f < 0.1) {
                    sourceFileOutput.write("    {1,1}");
                } else if (f < 0.55) {
                    sourceFileOutput.write("    {1,0}");
                } else {
                    sourceFileOutput.write("    {0,1}");
                }
                if (i < maxi - 1) {
                    sourceFileOutput.write(",\n");
                } else {
                    sourceFileOutput.write("\n};\n");
                }
            }
            sourceFileOutput.write("                  \n");
        } catch (IOException ex) {
            Logger.getLogger(InstanceGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    ///* spécialités des interfaces */
    //#define SPECIALITE_SANS       -1 
    //#define SPECIALITE_MENUISERIE  0 
    //#define SPECIALITE_ELECTRICITE 1 
    //#define SPECIALITE_MECANIQUE   2 
    //
    ///* specialite des interfaces */
    //int specialite_interfaces[NBR_INTERFACES][NBR_SPECIALITES]={
    //    {1,0,0},
    //    {0,1,0},
    //    {0,0,0},
    //    {0,0,1},
    //    {0,1,0},
    //    {0,0,0},
    //    {0,1,0},
    //    {1,0,0},
    //    {1,0,0},
    //    {0,1,0}
    //};
    private void writeSpecialiteInterfaces() {
        try {
            headerFileOutput.write("/* spécialités des interfaces */\n");
            headerFileOutput.write("#define SPECIALITE_SANS       -1 /* Enseigné dans le centre le plus proche */\n");
            for (int i=0;i<NBR_SPECIALITES;i++) {
                headerFileOutput.write("#define "+NOMS_SPECIALITES[i]+" "+i+"\n");
            }
            headerFileOutput.write("                  \n");

            sourceFileOutput.write("/* specialite des interfaces */\n");
            sourceFileOutput.write("int specialite_interfaces[NBR_INTERFACES][NBR_SPECIALITES]={\n");

            int maxi = NBR_INTERFACES;
            for (int i = 0; i < maxi; i++) {
                int maxj = NBR_SPECIALITES;
                sourceFileOutput.write("    {");
                for (int j = 0; j < maxj; j++) {
                    if (rand.nextDouble() < 0.2) {
                        sourceFileOutput.write("1");
                    } else {
                        sourceFileOutput.write("0");
                    }
                    if (j < maxj - 1) {
                        sourceFileOutput.write(",");
                    } else {
                        sourceFileOutput.write("}");
                    }
                }
                if (i < maxi - 1) {
                    sourceFileOutput.write(",\n");
                } else {
                    sourceFileOutput.write("\n};\n");
                }

            }
            sourceFileOutput.write("                  \n");

        } catch (IOException ex) {
            Logger.getLogger(InstanceGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    ///* coordonnées du centre, des interfaces et des apprenants */
    //float coord[NBR_NODES][2]=
    //{
    //    {565.0,575.0}, /* centre formation menuiserie */
    //    {25.0,185.0},  /* centre formation electricite */
    //    {345.0,750.0}, /* centre formation mecanique */
    //    {945.0,685.0}, /* point de départ interfaces */
    //    {845.0,655.0},
    //    {880.0,660.0},
    //    {25.0,230.0},
    //    {525.0,1000.0},
    //    {580.0,1175.0},
    //    {650.0,1130.0},
    //    {1605.0,620.0},
    //    {1220.0,580.0}, 
    //    {1465.0,200.0},
    //    {1530.0,5.0}, /* point de départ apprenants */
    //    {845.0,680.0},
    //    {725.0,370.0},
    //    {145.0,665.0},
    //    {415.0,635.0},
    //    {510.0,875.0},
    //    {560.0,365.0},
    //    {300.0,465.0}
    //} ;
    private void writeCoord() {
        try {
            sourceFileOutput.write("/* coordonnées des centres de formation, des interfaces et des apprenants */\n");
            sourceFileOutput.write("double coord[NBR_NODES][2]={\n");

            sourceFileOutput.write("                  \n");
            sourceFileOutput.write("    /* Les interfaces se rendent du centre SESSAD à l'école de formation */\n");

	    int maxi ;

            // Coord du centre
            maxi = 1;
            for (int i = 0; i < maxi; i++) {
                int x = (int) (rand.nextDouble() * DIMENSION_ZONE_GEOGRAPHIQUE);
                int y = (int) (rand.nextDouble() * DIMENSION_ZONE_GEOGRAPHIQUE);
                sourceFileOutput.write("    {" + x + "," + y + "}, /* centre " + i + " */\n");
            }

            sourceFileOutput.write("                  \n");
            sourceFileOutput.write("    /* Centres de formation */\n");

            int[] ALEACENTRES;
            ALEACENTRES = new int[NBR_CENTRES_FORMATION];
            for(int i = 0; i < NBR_CENTRES_FORMATION; i++) {
            	if(i < NBR_SPECIALITES) {
            		ALEACENTRES[i] = i;
            	}else {
            		ALEACENTRES[i] = rand.nextInt(NBR_SPECIALITES - 0) + 0;
            	}
            }
            Arrays.sort(ALEACENTRES);
            //System.out.println(Arrays.toString(NBR_CENTRES_FORMATION));

            // Coord des centres de formation
            maxi = NBR_CENTRES_FORMATION;
            for (int i = 0; i < maxi; i++) {
                int x = (int) (rand.nextDouble() * DIMENSION_ZONE_GEOGRAPHIQUE);
                int y = (int) (rand.nextDouble() * DIMENSION_ZONE_GEOGRAPHIQUE);
                sourceFileOutput.write("    {" + x + "," + y + "}, /* ecole formation " + NOMS_SPECIALITES[ALEACENTRES[i]] + " */\n");
            }

            sourceFileOutput.write("    /* Apprenants */\n");

            // Coord des apprenants
            maxi = NBR_APPRENANTS;
            for (int i = 0; i < maxi; i++) {
                int x = (int) (rand.nextDouble() * DIMENSION_ZONE_GEOGRAPHIQUE);
                int y = (int) (rand.nextDouble() * DIMENSION_ZONE_GEOGRAPHIQUE);
                sourceFileOutput.write("    {" + x + "," + y + "}");
                if (i < maxi - 1) {
                    sourceFileOutput.write(", /* apprenant " + i + " */\n");
                } else {
                    sourceFileOutput.write("/* apprenant " + i + " */\n};\n");
                }

            }

            headerFileOutput.write("\n");
            headerFileOutput.write("int list_center["+NBR_CENTRES_FORMATION+"];\n\n");

            sourceFileOutput.write("\n");
            sourceFileOutput.write("int list_center["+NBR_CENTRES_FORMATION+"] = {");
            for(int i = 0; i<NBR_CENTRES_FORMATION; i++){
                if(i == NBR_CENTRES_FORMATION-1){
                    sourceFileOutput.write(""+ALEACENTRES[i]);
                }else{
                    sourceFileOutput.write(ALEACENTRES[i]+", ");
                }
            }
            sourceFileOutput.write("};\n");
            sourceFileOutput.write("                  \n");

            sourceFileOutput.write("                  \n");
        } catch (IOException ex) {
            Logger.getLogger(InstanceGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //#define NBR_FORMATION          9
    //
    //#define LUNDI                  1
    //#define MARDI                  2
    //#define MERCREDI               3
    //#define JEUDI                  4
    //#define VENDREDI               5
    //#define SAMEDI                 6
    //
    ///* formation : apprenant, specialite, competence, horaire debut formation, horaire fin formation */
    //int formation[NBR_FORMATION][6]=
    //{
    //    {0,SPECIALITE_ELECTRICITE, COMPETENCE_SIGNES, LUNDI, 13, 16},
    //    {1,SPECIALITE_MENUISERIE, COMPETENCE_CODAGE, MARDI, 8, 11},
    //    {2,SPECIALITE_ELECTRICITE, COMPETENCE_CODAGE, MERCREDI, 9, 12},
    //    {3,SPECIALITE_MECANIQUE, COMPETENCE_SIGNES, MARDI, 14, 17},
    //    {4,SPECIALITE_ELECTRICITE, COMPETENCE_SIGNES, LUNDI, 9, 12},
    //    {5,SPECIALITE_MENUISERIE, COMPETENCE_SIGNES, JEUDI, 8, 11},
    //    {0,SPECIALITE_ELECTRICITE, COMPETENCE_SIGNES, JEUDI, 8, 12},
    //    {6,SPECIALITE_SANS, COMPETENCE_CODAGE, JEUDI, 14, 17},
    //    {7,SPECIALITE_ELECTRICITE, COMPETENCE_CODAGE, VENDREDI, 8, 11}
    //} ;    
    private void writeFormation() {
        try {
            headerFileOutput.write("#define NBR_FORMATION          " + NBR_FORMATIONS + "\n");
            headerFileOutput.write("                  \n");
            headerFileOutput.write("#define LUNDI                  1\n");
            headerFileOutput.write("#define MARDI                  2\n");
            headerFileOutput.write("#define MERCREDI               3\n");
            headerFileOutput.write("#define JEUDI                  4\n");
            headerFileOutput.write("#define VENDREDI               5\n");
            headerFileOutput.write("#define SAMEDI                 6\n");
            headerFileOutput.write("                  \n");

            sourceFileOutput.write("/* formation : id formation, specialite ou centre de formation, competence, horaire debut formation, horaire fin formation */\n");

            sourceFileOutput.write("int formations[NBR_FORMATION][6]={\n");

            int maxi = NBR_APPRENANTS;
            // public static int NBR_FORMATIONS = NBR_APPRENANTS * NBR_FORMATIONS_PAR_SEMAINE;
            for (int i = 0; i < maxi; i++) { // Apprenant i 
                // {0,SPECIALITE_ELECTRICITE, COMPETENCE_SIGNES, LUNDI, 13, 16},
                int maxj = NBR_FORMATIONS_PAR_SEMAINE ;
                for (int j = 0; j < maxj; j++) { // Apprenant i 
                    int spe = rand.nextInt(NBR_SPECIALITES);
                    String specialite = NOMS_SPECIALITES[spe];
                    String competence = NOMS_COMPETENCES[rand.nextInt(NBR_COMPETENCES)];
                    String jour = JOURS_SEMAINE[rand.nextInt(6)];
                    int matin = rand.nextInt(2);
                    int hdebut, hfin;
                    if (matin == 1) {
                        hdebut = 8 + rand.nextInt(3);
                        hfin = hdebut + rand.nextInt(11 - hdebut) + 2;
                    } else {
                        hdebut = 13 + rand.nextInt(4);
                        hfin = hdebut + rand.nextInt(18 - hdebut) + 2;

                    }
                    sourceFileOutput.write("   {" + i + "," + specialite + "," + competence + "," + jour + "," + hdebut + "," + hfin + "}");
                    if (i < maxi - 1 || j < maxj - 1) {
                        sourceFileOutput.write(",\n");
                    } else {
                        sourceFileOutput.write("\n};\n");
                    }
                }
            }

            sourceFileOutput.write("                  \n");
        } catch (IOException ex) {
            Logger.getLogger(InstanceGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

//int main() {
//    printf("NBR_INTERFACES=%d\n",NBR_INTERFACES) ;
//    printf("NBR_APPRENANTS=%d\n",NBR_APPRENANTS) ;
//    printf("NBR_NODES=%d\n",NBR_NODES) ;
//
//    return 0 ;
//}
    private void writeGlobal() {
        try {
            headerFileOutput.write("int competences_interfaces[NBR_INTERFACES][2];\n");
            headerFileOutput.write("                  \n");
            headerFileOutput.write("int specialite_interfaces[NBR_INTERFACES][NBR_SPECIALITES];\n");
            headerFileOutput.write("                  \n");
            headerFileOutput.write("double coord[NBR_NODES][2];\n");
            headerFileOutput.write("                  \n");
            headerFileOutput.write("int formations[NBR_FORMATION][6];\n");
        } catch (IOException ex) {
            Logger.getLogger(InstanceGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param string
     */
    private void log(String string) {
        System.out.println(string);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int arg0 = Integer.parseInt(args[0]); 
        int arg1 = Integer.parseInt(args[1]);
        int arg2 = Integer.parseInt(args[2]);
        int arg3 = Integer.parseInt(args[3]);
        InstanceGenerator instanceGenerator = new InstanceGenerator(arg0, arg1, arg2, arg3);
    }

}
