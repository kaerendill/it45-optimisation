var indexSectionsWithContent =
{
  0: "abcdefghijlmnoprstuvw",
  1: "acfil",
  2: "i",
  3: "acfgilmpr",
  4: "abcdefimopsu",
  5: "bcdefhijlmnpstvw",
  6: "acfil",
  7: "abcjlmnrsv",
  8: "r"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "defines",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Macros",
  8: "Pages"
};

