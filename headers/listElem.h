/**
 * @file listElem.h
 * @brief Element d'une liste.
 * @authors Théo Garrido & Enzo Rodrigues
 * @version 1.0
 * @date 22/05/2021
 */
#ifndef _LISTELEM_H_
#define _LISTELEM_H_

#include "./global.h"
#include <stdio.h>
#include <stdlib.h>

typedef struct listElem
{
    int idClient;
    int indexClient;
    int jourClient;
    int startingHour;
    int endingHour;
    struct listElem *next;
}listElem;


 /**
  * @brief Permet de créer un élément de liste.
  * @param idClient
  * @param indexClient
  * @param jourClient
  * @param startingHour
  * @param endingHour
  * @return
  */
listElem* createListElem(int idClient, int indexClient, int jourClient, int startingHour, int endingHour);

/**
 * printElem(), permet de créer un élément de liste
 */
/**
 * @brief Permet de créer un élément de liste.
 * @param self
 */
void printElem(listElem* self);

#endif