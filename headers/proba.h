/**
 * @file proba.h
 * @brief Toutes les fonctions en rapport avec les probabilités de choix de chemins.
 * @authors Théo Garrido & Enzo Rodrigues
 * @version 1.0
 * @date 22/05/2021
 */
#ifndef _PROBA_H_
#define _PROBA_H_

#include "./global.h"
#include "./ant.h"
#include <math.h>
#include <stdlib.h>


/**
 * @brief Calcul du denominateur pour la proba.
 * @param currentClient
 * @param currentDay
 * @param currentHour
 * @param currentSkill
 * @param pheromone
 * @param prob
 * @return
 */
double denoCalc(int currentClient, int currentDay, int currentHour, int currentSkill, double pheromone[NBR_NODES][NBR_NODES], double prob[NBR_NODES][NBR_NODES]);


/**
 * @brief Initialise la matrice de proba.
 * @param prob
 */
void initProbaMatrix(double prob[NBR_NODES][NBR_NODES]);


/**
 * @brief Update la matrice de proba.
 * @param currentClient
 * @param currentDay
 * @param currentHour
 * @param currentSkill
 * @param prob
 * @param pheromone
 */
void updateProbaMatrix(int currentClient, int currentDay, int currentHour, int currentSkill, double prob[NBR_NODES][NBR_NODES], double pheromone[NBR_NODES][NBR_NODES]);


/**
 * @brief Permet de trier **b** toujours supérieur a **a**.
 * @param a
 * @param b
 * @return
 */
int sortDecreasing(const void *a, const void *b);


/**
 * @brief Choisi un chemin en fonction d'un nombre compris entre {0,1}.
 * @param prob
 * @param cumulatif
 * @param currentClient
 */
void cumulativProba(double prob[NBR_NODES][NBR_NODES], coupleIndexValue cumulatif[NBR_NODES], int currentClient);


 /**
  * @brief Permet de savoir s'il reste des formations un jour donné.
  * @param startingI
  * @param currentDay
  * @param firstLineProb
  * @return
  */
int isRemainingFormationByDay(int startingI, int currentDay, double firstLineProb[NBR_NODES]);


 /**
  * @brief Permet de vérifier pour une ville i si elle est synchroniser avec une fourmi k pour un jour j.
  * @param indexClient
  * @param currentClient
  * @param currentDay
  * @param currentHour
  * @param currentSkill
  * @return
  */
int possibleWayAnt(int indexClient, int currentClient, int currentDay, int currentHour, int currentSkill);

#endif