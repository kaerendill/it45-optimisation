/**
 * @file formation.h
 * @brief Gère tout ce qui est en rapport avec les formations.
 * @authors Théo Garrido & Enzo Rodrigues
 * @version 1.0
 * @date 22/05/2021
 */

#ifndef CLONEGIT_FORMATIONS_H
#define CLONEGIT_FORMATIONS_H

#include "./global.h"

typedef struct formation{

    int indexClient;
    int specialite;
    int competence;
    int jour;
    int heureDepart;
    int heureFin;

}formation;


/**
 * @brief Trie les formations par heure et par jour par ordre croissant.
 * @param a
 * @param b
 * @return
 */
int sortIncreasingFormation(const void *a, const void *b);


 /**
  * @brief retourne la première formation du jour.
  * @param startingI
  * @param currentDay
  * @return
  */
int findFirstHourOfDay(int startingI, int currentDay);


 /**
  * @brief Permet d'obtenir la durée d'une formation.
  * @param startingI
  * @param currentClient
  * @param currentDay
  * @param currentHour
  * @return
  */
int durationFormation(int startingI, int currentClient, int currentDay, int currentHour);


/**
 * @brief Permet de retourner l'index où commencent les formations d'un jour donné.
 * @param startingI
 * @param currentDay
 * @return
 */
int findIndexFirstFormationDay(int startingI, int currentDay);


/**
 * @brief return une formation.
 * @param startingI
 * @param idClient
 * @return
 */
int *findFormation(int startingI, int idClient);

#endif
