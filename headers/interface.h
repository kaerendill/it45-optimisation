/**
 * @file interface.h
 * @brief Gestion des interfaces en lisant les fichiers générés **instance-formation**.
 * @authors Théo Garrido & Enzo Rodrigues
 * @version 1.0
 * @date 22/05/2021
 */

#ifndef CLONEGIT_INTERFACE_H
#define CLONEGIT_INTERFACE_H

#include <math.h>
#include <string.h>
#include "./list.h"
#include "./global.h"

typedef struct interface
{
    int id;
    int skills[NBR_SKILLS];
    int specialities[NBR_SPECIALITES];
    int totalWorkHour;
    double totalDistance;
    int workingDays[6];
    list path;

}interface;


/**
 * @brief Permet de cree une interface depuis le fichier **instance-formation**.
 * @param id
 * @param skills
 * @param spes
 * @return
 */
interface createInterface(int id, int skills[NBR_SKILLS], int spes[NBR_SPECIALITES]);



 /**
  * @brief Affiche les interfaces.
  * @param anInterfaces
  */
void printInterfaces(interface anInterfaces[NBR_INTERFACES]);

/**
 * @brief Affiche les interfaces eyant un chemins.
 * @return le nombre d'interface sans chemins.
 */
int printInterfacesWithPath(interface anInterfaces[NBR_INTERFACES]);


 /**
  * @brief Ajoute chaque interface dans le tableau interfaces.
  * @param interfaces
  */
void initInterfaces(interface interfaces[NBR_INTERFACES]);

#endif //CLONEGIT_INTERFACE_H
