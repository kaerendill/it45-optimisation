/**
 * @file ant.h
 * @brief Une ant (ou fourimi) représente tous les déplacements faisable par les interfaces. Contient la structure d'une ANT.
 * @authors Théo Garrido & Enzo Rodrigues
 * @version 1.0
 * @date 22/05/2021
 */

#ifndef _ANT_H_
#define _ANT_H_

#include "./global.h"
#include "./proba.h"
#include "./list.h"
#include "./formation.h"
#include "./interface.h"


typedef struct ant
{
    list path;
    int startingI;
    int currentClient;
    int targetClient;
    int totalWorkHour;
    int endFormationHour;
    int currentDay;
    int currentHour;
    int currentSkill;
    double totalDist;
    double moyenne;
    double ecartType;
    double facCorre;
    int penaSpe;
    double evaluation;
} ant;


/**
 * @brief Permet d'initialiser une ANT correctement.
 * @param self  ANT à initialiser.
 */
void createAnt(ant *self);


/**
 * @brief Faire le premier choix entre les différents clients grace au tableau cumulatif[].
 * @param ant           ANT sur laquel on veut effectuer le choix.
 * @param cumulatif     Tableau de probabilité des chemins possible.
 * @param currentDay    Permet de vérifier dans quel jour on se trouve pour eviter les erreurs.
 * @return              Client vers lequel la fourmi va se rendre.
 */
int doFirstChoice(ant *ant, coupleIndexValue cumulatif[NBR_NODES], int currentDay);


/**
 * @brief Faire un choix de déplacement qui n'est pas le premier, entre les différents clients grace au tableau cumulatif[]. Va modifier le ant.targetClient, ant->totalWorkHour, ant->currentHour, ant->targetClient.
 * @param ant           ANT sur laquel on veut effectuer le choix.
 * @param cumulatif     Tableau de probabilité des chemins possible.
 */
void doChoice(ant *ant, coupleIndexValue cumulatif[NBR_NODES]);


/**
 * @brief Permet de faire bouger l'ANT, c'est à dire modifier le ant.path qui est le chemins que l'ANT va effectuer.
 * @param ant   ANT qu'on veut déplacer.
 * @param prob  Tableau de probabilité de déplacement d'un client à un autre.
 */
void moveAnt(ant *ant, double prob[NBR_NODES][NBR_NODES]);


 /**
  * @brief  Créer un chemins pour une ANT en utilisant **doChoice()** et **moveAnt()**.
  * @param ant          ANT qu'on veut utiliser pour un déplacement.
  * @param prob         Matrice de probabilités
  * @param pheromone    Matrice de pheromone
  * @param cumulatif    Tableau qui va être généré grace à la matrice **prob[][]**
  * @return             ANT modifié avec un chemins créé.
  */
ant createWay(ant *ant, double prob[NBR_NODES][NBR_NODES], double pheromone[NBR_NODES][NBR_NODES], coupleIndexValue cumulatif[NBR_NODES]);


 /**
  * @brief Le path final d'une ANT va être divisé et redistribué aux interfaces en prenant en compte leurs compétences, spécialités, temps de travail global et aussi en s'assurant que l'interface ne soit pas déjà avec un client.
  * @param ant          ANT final.
  * @param interfaces   Tableau d'interfaces.
  * @return             Return 1 si la solution est faisable.
  */
int buildSolution(ant *ant, interface interfaces[NBR_INTERFACES]);


/**
 * @brief Met à jour les infos de la ANT sur ses compétences, son heure actuel et son nombre d'heure de travail cumulé.
 * @param ant   ANT à mettre à jour.
 * @return      Return 1 si cette mise à jour est possible.
 */
int setHourAndSkillAnt(ant *ant);


 /**
  * @brief Permet d'affecter un sous chemin du path final de l'ANT à une interface à l'aide de **calculScore()**.
  * @param interfaces       Tableau de toutes les interfaces.
  * @param distanceSubPath  Distance du sous chemins qui va être ajouté à **totalDist** de l'interface.
  * @param currentSkill     La compétence actuelle du sous-chemin.
  * @param totalWorkHour    Total du temps de travail a effectué pour ce sous-chemin.
  * @param eachSpe          Tableau de toutes les spécialités possibles.
  * @param subPath          Liste des déplacements (le sous chemin).
  * @param day              Jour actuel du sous chemin.
  * @param penaCount        Compteur de spécialité non satisfaites.
  * @return                 Return le nombre de spécialités non cohérente avec l'interface.
  */
int affectSubPathWithInterface(interface interfaces[NBR_INTERFACES], int distanceSubPath, int currentSkill, int totalWorkHour, int eachSpe[NBR_SPECIALITES], list subPath, int day, int *penaCount);

/**
 * @brief Permet de trouver le centre avec la bonne compétence le plus proche du client.
 * @param currentClientIndex
 * @param currentSpe
 * @return retourne l'indexe du centre
 */
int findNearestCenter(int currentClientIndex, int currentSpe);

 /**
  * @brief Permet de calculer le score d'une interface c'est à dire, à quel point une interface est bonne pour un sous chemins.
  * @param interface    Interface à tester.
  * @param currentSkill La compétence actuelle du sous-chemin.
  * @param eachSpe      Tableau de toutes les spécialités possibles.
  * @param day          Jour actuel du sous chemin.
  * @return             Score de l'interface testé.
  */
int calculScore(interface interface, int currentSkill, int eachSpe[NBR_SPECIALITES], int day);


 /**
  * @brief Renvois la distance moyenne parcourue par toutes les interfaces de la solution.
  * @param distanceTotal    Distance totale parcourue par les interfaces.
  * @return                 La moyenne obtenue.
  */
double calcMoy(double distanceTotal);


 /**
  * @brief Renvois l'ecart type des distances de la solution.
  * @param moyenneDistance  Distance moyenne parcourue par toutes les interfaces de la solution.
  * @param interfaces       Tableau des interfaces.
  * @return                 L'écart type calculé.
  */
double calcEcartType(double moyenneDistance,interface interfaces[NBR_INTERFACES]);


 /**
  * @brief Renvois le facteur de corrélation de la solution.
  * @param distanceTotal    Distance totale parcourue par les interfaces.
  * @return                 Facteur de corrélation calculé.
  */
double calcFacCorre(double distanceTotal);


/**
 * @brief Calcule de l'evaluation (minZ) d'une ANT.
 * @param moyenne
 * @param ecartType
 * @param facCorre
 * @param penalite
 * @return          minZ
 */
double calculEvaluation(double moyenne, double ecartType, double facCorre, int penalite);

#endif