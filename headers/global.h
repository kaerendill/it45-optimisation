/**
 * @file global.h
 * @brief Gestion de variables.
 * @authors Théo Garrido & Enzo Rodrigues
 * @version 1.0
 * @date 22/05/2021
 */

#ifndef _GLOBAL_H_
#define _GLOBAL_H_

#include "../data/instance-formations.h"

#define NBR_SESSAD 1
#define NBR_SKILLS 2
#define A 0.5
#define B 0.1
#define RHO 0.5
#define MINPHE 0.00001

/* Distance matrix */
double dist[NBR_NODES][NBR_NODES];

int formationsCopy[NBR_FORMATION][6];


typedef struct coupleIndexValue
{
  int index;
  double value;
  
}coupleIndexValue;

#endif
