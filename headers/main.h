/**
 * @file main.h
 * @brief Fonctions de base.
 * @authors Théo Garrido & Enzo Rodrigues
 * @version 1.0
 * @date 22/05/2021
 */
#ifndef _MAIN_H_
#define _MAIN_H_

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include "./global.h"
#include "./list.h"
#include "./proba.h"
#include "./ant.h"
#include "./colony.h"
#include "./formation.h"
#include "./interface.h"

/**
 * @brief Affiche une matrice.
 * @param d    Matrice a afficher.
 */
void print_matrix(double d[NBR_NODES][NBR_NODES]);


 /**
  * @brief Permet d'afficher un tableau en fonction de sa taille.
  * @param array
  * @param size
  */
void printArray(int array[], int size);


/**
 * @brief Permet de calculer les distances entres toutes les nodes.
 * @param d
 * @param coord
 */
void calc_matrix(double d[NBR_NODES][NBR_NODES], double coord[NBR_NODES][2]);

#endif