/**
 * @file list.h
 * @brief Liste pointe vers le premier **listElem**.
 * @authors Théo Garrido & Enzo Rodrigues
 * @version 1.0
 * @date 22/05/2021
 */
#ifndef _LIST_H_
#define _LIST_H_

#include "./global.h"
#include <stdio.h>
#include "./listElem.h"

typedef listElem* list;


/**
 * @brief Permet d'ajouter un élément en queue.
 * @param self
 * @param elem
 * @return
 */
list addTail(list self, listElem *elem);

/**
 * printList(), permet d'afficher une liste
 */
/**
 * @brief Permet d'afficher une liste.
 * @param self
 */
void printList(list self);


/**
 * @brief Permet de récupérer un client par son index.
 * @param self
 * @param index
 * @return
 */
int findClientByIndex(list self, int index);

#endif