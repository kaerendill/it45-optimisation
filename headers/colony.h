/**
 * @file colony.h
 * @brief Permets de gérer toutes les ANT Générés et donc de récupérer la meilleure des ANT qui a la meilleure solution minZ.
 * @authors Théo Garrido & Enzo Rodrigues
 * @version 1.0
 * @date 22/05/2021
 */

#ifndef _COLONY_H_
#define _COLONY_H_

#include <string.h>
#include "./global.h"
#include "./interface.h"
#include "./ant.h"

typedef struct colony
{
    int nbAnt;
    ant bestAnt;
    interface bestInterfaces[NBR_INTERFACES];
    double pheromone[NBR_NODES][NBR_NODES];

}colony;


 /**
  * @brief Permet de créer une colonie.
  * @param nbAnt    Nombre de ANT qu'on va générer.
  * @return         Une colonie de ANT.
  */
colony createColony(int nbAnt);


 /**
  * @brief Évapore les phéromones.
  * @param pheromone    Matrice de pheromone que laisse une fourmis d'un point A à B après son passage.
  */
void evaporatePheromone(double pheromone[NBR_NODES][NBR_NODES]);


 /**
  * @brief Mets à jour la matrice des phéromones.
  * @param ant
  * @param pheromone
  */
void updatePheromone(struct ant ant, double pheromone[NBR_NODES][NBR_NODES]);


/**
 * @brief Lance l'optimisation.
 * @param colony
 */
void optimization(colony *colony);

#endif