#include <stdio.h>
                  
#define NBR_INTERFACES        38
#define NBR_APPRENANTS        131
#define NBR_FORMATIONS        131
#define NBR_CENTRES_FORMATION 15
#define NBR_SPECIALITES       8
#define NBR_NODES 	      NBR_CENTRES_FORMATION+NBR_INTERFACES+NBR_APPRENANTS
                  
/* code des compétence en langage des signes et en codage LPC */
#define COMPETENCE_SIGNES     0
#define COMPETENCE_CODAGE     1
                  
/* spécialités des interfaces */
#define SPECIALITE_SANS       -1 /* Enseigné dans le centre le plus proche */
#define SPECIALITE_MENUISERIE 0
#define SPECIALITE_ELECTRICITE 1
#define SPECIALITE_MECANIQUE 2
#define SPECIALITE_INFORMATIQUE 3
#define SPECIALITE_CUISINE 4
#define SPECIALITE_COMMERCIAL 5
#define SPECIALITE_OENOLOGIE 6
#define SPECIALITE_PLOMBERIE 7
                  

int list_center[15];

#define NBR_FORMATION          131
                  
#define LUNDI                  1
#define MARDI                  2
#define MERCREDI               3
#define JEUDI                  4
#define VENDREDI               5
#define SAMEDI                 6
                  
int competences_interfaces[NBR_INTERFACES][2];
                  
int specialite_interfaces[NBR_INTERFACES][NBR_SPECIALITES];
                  
double coord[NBR_NODES][2];
                  
int formations[NBR_FORMATION][6];
